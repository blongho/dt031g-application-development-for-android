package se.miun.lobe1602.bathingsites.helpers;

import android.arch.persistence.room.Ignore;

/**
 * A subset of bathing site info.
 */
public class GoogleMapData {
    private String name;
    private String address;
    private double longitude;
    private double latitude;

    @Ignore
    public GoogleMapData() {}

    public GoogleMapData(final String name, final String address, final double longitude,
                         final double latitude) {
        this.name = name;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "[Name: " + name + "\naddr.: " + address + "\n(lat|long: " +
            latitude + "|" + longitude + ")]";
    }

    /**
     * Small summary to display on the map
     * @return name, latitude and longitude of the site
     */
    public String summary() {
        String summary = name + " [";
        summary += latitude + ", " + longitude + "]";
        return summary;
    }
}