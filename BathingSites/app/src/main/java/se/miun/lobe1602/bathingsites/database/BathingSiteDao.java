package se.miun.lobe1602.bathingsites.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import se.miun.lobe1602.bathingsites.helpers.BathingSite;
import se.miun.lobe1602.bathingsites.helpers.Globals;
import se.miun.lobe1602.bathingsites.helpers.GoogleMapData;

@Dao
public interface BathingSiteDao {
    @Insert(onConflict = OnConflictStrategy.FAIL)
    long insert(BathingSite bathingSite);

    @Query("DELETE FROM " + Globals.DATABASE_TABLE)
    void deleteAllDates();

    @Query("SELECT * FROM " + Globals.DATABASE_TABLE + " ORDER BY name ASC")
    List<BathingSite> getAllSites();

    @Query("SELECT name, address, longitude, latitude FROM " + Globals.DATABASE_TABLE + " WHERE " +
               "longitude IS NOT NULL AND latitude IS NOT NULL")
    List<GoogleMapData> getGoogleMapData();

    @Query("SELECT * FROM " + Globals.DATABASE_TABLE + " WHERE name = :siteName")
    BathingSite getSiteByName(String siteName);

    @Delete
    int deleteSites(BathingSite... sites);

}

