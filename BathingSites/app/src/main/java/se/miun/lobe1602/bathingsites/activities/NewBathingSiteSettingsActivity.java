package se.miun.lobe1602.bathingsites.activities;
/**
 * @file NewBathingSiteSettingsActivity.java <br>
 *     <p>
 *     Main settings for the app. This class is made just to create a uniform user experience with
 *     the up button
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-05-26
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import se.miun.lobe1602.bathingsites.preference.SettingsFragment;

public class NewBathingSiteSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().addToBackStack(null)
            .replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
