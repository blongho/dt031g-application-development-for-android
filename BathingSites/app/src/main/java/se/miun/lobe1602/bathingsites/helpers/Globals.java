/**
 * Globals.java
 * @author Bernard Che Longho(lobe1602)
 *     <p>
 *     A helper class to store all global constants and methods to be used across the app
 * @since 2018-05-23
 */
package se.miun.lobe1602.bathingsites.helpers;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class Globals {
    public static final int FILE_WRITE_PERMISSION_CODE = 20;
    public static final int NETWORK_ACCESS_CODE = 10;
    public static final String BATHING_SITES = "sites";
    public static final String BASE_URL_WEATHER = "weather_location";
    public static final String DEFAULT_COUNTRY_LANGAGE = "language";
    public static final String DATABASE_NAME = "bathing_sites";
    public static final String DATABASE_TABLE = "bathing_sites_table";
    public static final String BATHING_SITES_URL = "site_location";
    public static final int DATABASE_VERSION = 1;
    public static final String BATHING_SITES_FILE = "downloadedSite.csv";
    public static final String SITE_RADIUS = "site_radius";

    public static int NUMBER_OF_BATHING_SITES = 0;


    public Globals() {}

    /**
     * Save a value to the SharedPreferences
     * @param key the key to identify the preference
     * @param value the value of the preference
     * @param context the application context (activity)
     */
    public static void addToPreference(String key, String value, Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Retrieve a stored preference
     * @param key the key to identify the preference value
     * @param context the application context
     * @return the string representation of the preference value
     */
    public static String getPreference(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean hasNetworkConnection(Application application) {
        ConnectivityManager connectivityManager =
            (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static boolean deleteFile() {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .toString();
        File file = new File(path);
        Boolean deleted = false;
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                if (f.getName().startsWith("downloadedSite")) {
                    deleted = f.delete();
                }
            }
        }
        return deleted;
    }

    public static void toastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    /**
     * Shoe the progress bar while some async task is occuring
     * @param context the application context
     * @param title the title of the progress bar
     * @param message the message to display
     */
    public Dialog showProcessEndDialog(Context context, String title, String message) {
        Dialog pd = new Dialog(context);
        pd.setTitle(title);
        final TextView view = new TextView(context);
        view.setTextSize(18f);
        view.setText(message);
        pd.setContentView(view);
        pd.setCancelable(true);
        return pd;
    }


}
