package se.miun.lobe1602.bathingsites.helpers;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewHelper extends WebViewClient {
    @Override
    public void onPageStarted(final WebView view, final String url, final Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(final WebView view, final String url) {
        super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedError(final WebView view, final int errorCode, final String description,
                                final String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        Log.e("Error", description);
    }

    @Override
    public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
        view.loadUrl(url);
        return true;
    }

    public WebViewHelper() {
        super();
    }
}
