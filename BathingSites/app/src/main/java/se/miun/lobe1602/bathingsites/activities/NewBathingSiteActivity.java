package se.miun.lobe1602.bathingsites.activities;
/**
 * @file NewBathingSiteActivity.java <br>
 *     <p>
 *     the system are shown to the user and the user has the opportunity to add new bathing sites
 *     and to make some settings in the settings menu.
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-05-26
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import se.miun.lobe1602.bathingsites.R;
import se.miun.lobe1602.bathingsites.database.BathingSiteRepository;
import se.miun.lobe1602.bathingsites.helpers.BathingSite;
import se.miun.lobe1602.bathingsites.helpers.Globals;
import se.miun.lobe1602.bathingsites.views.BathingSitesView;

public class NewBathingSiteActivity extends AppCompatActivity {

    private String name, description, address, date;
    private double longitude, latitude, rating, temperature;
    private TextView name_field, address_field, desc_field, long_field, lat_field, temp_field,
        date_field;
    private RatingBar ratingBar;
    private BathingSitesView bathingSitesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.settings_preferences, false);
        setContentView(R.layout.activity_new_bathing_site);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        longitude = latitude = rating = temperature = 0;
        long_field = byId(R.id.input_longitude);
        lat_field = byId(R.id.input_latitude);
        name_field = byId(R.id.input_name);
        desc_field = byId(R.id.input_description);
        temp_field = byId(R.id.input_temp);
        address_field = byId(R.id.input_address);
        ratingBar = (RatingBar) findViewById(R.id.input_rating);
        date_field = byId(R.id.input_date);
        bathingSitesView = (BathingSitesView) findViewById(R.id.bathing_site_view);
        date_field.setFocusable(false);
        date_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                chooseDate();
            }
        });
    }

    /**
     * Get an EditText view by id
     * @param id id of the view
     * @return the view
     */
    private EditText byId(int id) {
        return (EditText) findViewById(id);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Globals.addToPreference(Globals.BATHING_SITES,
            Integer.toString(bathingSitesView.getNumberOfBathingSites()), this);

    }

    // get preferences here
    @Override
    protected void onResume() {
        super.onResume();
        String sites = Globals.getPreference(Globals.BATHING_SITES, this);
        if (!sites.isEmpty()) {
            int bathingSites = Integer.valueOf(sites);
            bathingSitesView.setNumberOfBathingSites(bathingSites);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_bathing_site, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(this, NewBathingSiteSettingsActivity.class));
                return true;
            case R.id.menu_save:
                doSaveStuff();
                return true;
            case R.id.menu_clear:
                clearValues();
                return true;
            case R.id.menu_weather:
                doWeatherStuff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    /**
     * Check if compulsory information has been filled
     * @return true if name & address or name && coordinates has been filled
     */
    private boolean hasNecessaryInformation() {
        String name = name_field.getText().toString();
        String location = address_field.getText().toString();
        String lon = long_field.getText().toString();
        String lat = lat_field.getText().toString();

        if (!name.isEmpty() && !location.isEmpty()) {
            return true;
        } else if (!name.isEmpty() && !lon.isEmpty() && !lat.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Get the values entered in the fragment
     */
    private void setValues() {
        name = name_field.getText().toString();
        description = desc_field.getText().toString();
        address = address_field.getText().toString();

        String long_text = long_field.getText().toString();
        if (!long_text.isEmpty()) {
            longitude = Double.valueOf(long_text);
        }

        String lat_text = lat_field.getText().toString();

        if (!lat_text.isEmpty()) {
            latitude = Double.valueOf(lat_text);
        }
        rating = ratingBar.getRating();

        String tmp_text = temp_field.getText().toString();
        if (!tmp_text.isEmpty()) {
            temperature = Double.valueOf(tmp_text);
        }
        date = date_field.getText().toString();

    }

    /**
     * Check the compulsory fields and set errors as needed.
     */
    private void checkAndSetErrors() {
        if (name_field.getText().toString().isEmpty()) {
            name_field.setError(getResources().getString(R.string.error_name_not_filled));
        }
        if (address_field.getText().toString().isEmpty()) {
            address_field.setError(getResources().getString(R.string.error_address_not_filled));
        }

        if (long_field.getText().toString().isEmpty()) {
            long_field.setError(getResources().getString(R.string.error_longitude_not_filled));
        }

        if (lat_field.getText().toString().isEmpty()) {
            lat_field.setError(getResources().getString(R.string.error_latitude_not_filled));
        }
    }

    /**
     * Clear all input fields. Done when the clear menu item is chosen
     */
    private void clearValues() {
        // Clear all input texts
        name_field.setText(null);
        desc_field.setText(null);
        address_field.setText(null);
        long_field.setText(null);
        lat_field.setText(null);
        ratingBar.setRating(0f);
        temp_field.setText(null);
        date_field.setText(null);

        // if there are any error messages, clear them
        clearErrors();
    }

    /**
     * Get The weather information based on the entries <br> First, if there is no network
     * connection, tell the user and do not proceed<br>
     * <p>
     * If the user has supplied both the address and the location values, use the location values
     */
    private void doWeatherStuff() {
        if (!canGetWeather()) {
            setWeatherErrors();
            Globals.toast(this, "Please field the fields marked with error");
        } else {
            try {
                if (Globals.hasNetworkConnection(getApplication())) {
                    String weatherURL = Globals.getPreference(Globals.BASE_URL_WEATHER, this);
                    setValues();
                    final WeatherDownloadAsyncTask weatherDownload =
                        new WeatherDownloadAsyncTask(NewBathingSiteActivity.this);

                    if (hasValuesForLocation()) {
                        weatherDownload.execute(weatherURL, String.valueOf(longitude),
                            String.valueOf(latitude));
                    } else if (!address.isEmpty()) {
                        weatherDownload.execute(weatherURL, address);
                    } else {
                        Globals.toast(this, "You must provide either address or coordinates");
                    }

                } else {
                    Globals.toast(this, "Oops, No network connection");

                }
            } catch (NullPointerException e) {
                Globals.toast(NewBathingSiteActivity.this, "Error: " + e.getMessage());
                //Log.i(getClass().getSimpleName().toUpperCase(), e.getMessage());
            }
        }
    }

    private void doSaveStuff() {
        if (hasNecessaryInformation()) {
            setValues();
            BathingSite bathingSite =
                new BathingSite(name, address, description, longitude, latitude, rating,
                    temperature, date);

            BathingSiteRepository repository = new BathingSiteRepository(NewBathingSiteActivity
                .this);

            // Check if bathing site has been saved.
            if (!repository.siteAlreadyExits(bathingSite)) { // bathing site is not already saved
                Globals.toast(NewBathingSiteActivity.this, "Saving the user data");

                repository.insert(bathingSite); // attempt    saving
                showProcessEndDialog("Success",
                    "Bathing site '" + bathingSite.getName() + "' saved", true);

                clearErrors();
                clearValues();

            } else { // attempting to save a site that already exists
                showProcessEndDialog("Failure",
                    "Bathing site '" + bathingSite.getName() + "' NOT saved. Check the values of" +
                        " the latitude and longitude", false);
                lat_field.setTextColor(Color.RED);
                long_field.setTextColor(Color.RED);
                checkAndSetErrors();
            }

            // get the current number of bathing sites
            Globals.NUMBER_OF_BATHING_SITES = repository.numberOfBathingSites();

            // update the bathing site views
            bathingSitesView.setNumberOfBathingSites(Globals.NUMBER_OF_BATHING_SITES);

            // save this new value into the preference
            Globals.addToPreference(Globals.BATHING_SITES,
                Integer.toString(Globals.NUMBER_OF_BATHING_SITES), NewBathingSiteActivity.this);
        } else {
            checkAndSetErrors();
            Globals.toast(NewBathingSiteActivity.this, "Fill in the neccessary fields");
        }
    }

    /**
     * Check if all fields have been saved
     * @return true if all the fields have a value
     */
    private boolean allFieldsHasValue() {
        return !name_field.getText().toString().isEmpty() &&
            !desc_field.getText().toString().isEmpty() &&
            !address_field.getText().toString().isEmpty() &&
            !date_field.getText().toString().isEmpty() && hasValuesForLocation() &&
            !temp_field.getText().toString().isEmpty() && rating > 0;
    }

    /**
     * Clear any errors that might exists on the screen
     */
    private void clearErrors() {
        name_field.setError(null);
        address_field.setError(null);
        long_field.setError(null);
        lat_field.setError(null);
    }

    /**
     * Check if weather parameters have been entered [address | (longitude&& latitude)]
     * @return true if either the address field or the location data [longitude && latitude] exists
     */
    private boolean canGetWeather() {
        return hasValuesForLocation() || !address_field.getText().toString().isEmpty();
    }

    private void setWeatherErrors() {
        if (address_field.getText().toString().isEmpty()) {
            address_field.setError(getResources().getString(R.string.error_address_not_filled));
        }

        if (long_field.getText().toString().isEmpty()) {
            long_field.setError(getResources().getString(R.string.error_longitude_not_filled));
        }

        if (lat_field.getText().toString().isEmpty()) {
            lat_field.setError(getResources().getString(R.string.error_latitude_not_filled));
        }
    }

    /**
     * Check that atleast one value for the location is not entered
     * @return true if one of longitude or latitude is missing
     */
    private boolean hasValuesForLocation() {
        return !long_field.getText().toString().isEmpty() &&
            !lat_field.getText().toString().isEmpty();
    }

    private void showProcessEndDialog(String title, String message, final boolean toMain) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message)
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (toMain) {
                        dialog.dismiss();
                        finish();
                    }
                    dialog.dismiss();
                }
            }).show();
    }

    /**
     * Code snippet to pick a date from a view click
     */
    private void chooseDate() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker =
            new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(final DatePicker view, final int year, final int month,
                                      final int dayOfMonth) {

                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf =
                        new SimpleDateFormat("yyyy-MM-dd");
                    calendar.set(year, month, dayOfMonth);
                    String dateString = sdf.format(calendar.getTime());
                    date_field.setText(dateString);
                }
            }, year, month, day); // set date picker to current date

        datePicker.show();

        datePicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(final DialogInterface dialog) {
                dialog.dismiss();
            }

        });
    }

    /**
     * Async task to download weather information
     */
    private class WeatherDownloadAsyncTask extends AsyncTask<String, String, ArrayList<String>> {
        private String temp;
        private String condition;
        private String imgLink;
        private String address;
        private ProgressDialog pd;
        private Context context;

        WeatherDownloadAsyncTask(Context context) {
            this.context = context;
            pd = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Weather download");
            pd.setMessage("Getting current weather...");
            pd.setCanceledOnTouchOutside(false);
            pd.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(final DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
            pd.show();
        }

        @Override
        protected ArrayList<String> doInBackground(final String... strings) {
            String url = "";
            if (strings.length > 2) {
                url = makeURLwithLocation(strings[0], strings[1], strings[2]);
                //Log.d("FILE DOWNLOAD URL", url);
            } else {
                url = makeURLwithAddress(strings[0], strings[1]);
                //Log.d("FILE DOWNLOAD URL", url);
            }

            InputStream in;

            try {
                URL myURL = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("GET");
                conn.connect();
                in = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    if (isCancelled()) {
                        publishProgress("Process cancelled prematurely");
                        break;
                    }
                    String[] splits = line.split(":");
                    if (splits[0].equalsIgnoreCase("address")) {
                        address = splits[1].replace("<br>", "");
                    }
                    if (splits[0].equalsIgnoreCase("condition")) {
                        condition = splits[1].replace("<br>", "");
                    }
                    if (splits[0].equalsIgnoreCase("temp_c")) {
                        temp = splits[1].replace("<br>", "");
                    }
                    if (line.startsWith("image")) {
                        imgLink = line.substring(line.indexOf(":") + 1, line.length())
                            .replace("<br>", "");
                    }
                }
                in.close();
                conn.disconnect();
                reader.close();
                final ArrayList<String> values = new ArrayList<>();
                values.add(0, imgLink);
                values.add(1, condition);
                values.add(2, temp);
                values.add(3, address);
                return values;
            } catch (Exception e) {
                Log.i("GETTING WEATHER FILE", e.getMessage());
                //Toast.makeText(NewBathingSiteActivity.this,
                //"Something went wrong! Weather download impossible", Toast.LENGTH_LONG)
                // .show();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(final String... values) {
            super.onProgressUpdate(values);
            pd.setMessage(values[0]);
        }

        /**
         * After completion, replace the progrebar with a dialog showing the weather information
         * @param s an arraylist of strings
         */
        @Override
        protected void onPostExecute(final ArrayList<String> s) {
            if (!s.get(2).trim().equalsIgnoreCase("null")) {
                publishProgress("Processing weather image...");
                final Dialog dialog = new Dialog(NewBathingSiteActivity.this);
                dialog.setContentView(R.layout.weather_info);
                final TextView cond = (TextView) dialog.findViewById(R.id.weather_description);
                cond.setText(s.get(1));

                final TextView temperature =
                    (TextView) dialog.findViewById(R.id.weather_temperature);
                temperature.setText(s.get(2));

                ImageView weatherImage = (ImageView) dialog.findViewById(R.id.weather_image);

                // Redirect activity to another thread which gets the weather image from the url
                DownloadImageAsyncTask d = new DownloadImageAsyncTask();
                try {
                    Drawable drawable = d.execute(s.get(0)).get();
                    if (drawable != null) {
                        weatherImage.setImageDrawable(drawable);
                    } else { // put a fallback image if image was not obtained
                        weatherImage.setImageResource(R.mipmap.ic_weather);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                Button button = (Button) dialog.findViewById(R.id.closeWeatherDialog);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        dialog.dismiss();

                    }
                });
                pd.dismiss();
                dialog.show();
            } else {
                pd.dismiss();
                Globals.toast(context, "Weather could not be obtained. ");
            }

        }

        /**
         * Make url from location data
         * @param base the base url => Weather download location
         * @param longitude the longitude
         * @param latitude the latitude
         * @return cancatenated url with base?location=longitude|latitude&language=EN
         */
        private String makeURLwithLocation(String base, String longitude, String latitude) {
            return base + "?location=" + longitude.trim() + "|" + latitude.trim() + "&language=EN";
        }

        /**
         * Make a url from
         * @param base the base url => Weather download location
         * @param address the city
         * @return concatenated full address to fetch
         */
        private String makeURLwithAddress(String base, String address) {
            return base + "?location=" + address.trim() + "&language=EN";
        }

    }

    /**
     * A small async task to download and create the weather image
     */
    private static class DownloadImageAsyncTask extends AsyncTask<String, Void, Drawable> {
        @Override
        protected Drawable doInBackground(final String... strings) {
            String s1 = strings[0]; // get the data from the main activity's execute parameter
            InputStream inputStream;
            try {
                URL url = new URL(s1);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000); // 10 seconds for reading
                conn.setConnectTimeout(20000); // 20 seconds for connecting
                conn.setRequestMethod("GET");
                conn.connect();

                inputStream = conn.getInputStream();

                Drawable drawable = Drawable.createFromStream(inputStream, "src");
                return drawable;
            } catch (Exception e) {
                Log.i(getClass().getSimpleName().toUpperCase(), e.getMessage());

            }
            return null;
        }
    }
}
