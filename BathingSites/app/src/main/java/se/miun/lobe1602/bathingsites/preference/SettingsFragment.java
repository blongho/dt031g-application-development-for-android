package se.miun.lobe1602.bathingsites.preference;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import se.miun.lobe1602.bathingsites.R;

public class SettingsFragment extends PreferenceFragment
    implements OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_preferences);

        // show the current values in the settings screen
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
            showSummary(getPreferenceScreen().getPreference(i));
        }
    }

    private void showSummary(final Preference preference) {
        if (preference instanceof PreferenceCategory) {
            PreferenceCategory category = (PreferenceCategory) preference;
            for (int i = 0; i < category.getPreferenceCount(); i++) {
                showSummary(category.getPreference(i));
            }
        } else {
            updatePreferences(preference);
        }
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences,
                                          final String key) {

        updatePreferences(findPreference(key));
    }

    /**
     * Update the preferences
     * @param preference current value
     */
    private void updatePreferences(final Preference preference) {
        if (preference instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) preference;
            preference.setSummary(editTextPref.getText());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
            .unregisterOnSharedPreferenceChangeListener(this);
    }


}
