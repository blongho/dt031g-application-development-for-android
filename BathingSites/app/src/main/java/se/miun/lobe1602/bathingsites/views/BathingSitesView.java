package se.miun.lobe1602.bathingsites.views;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import se.miun.lobe1602.bathingsites.R;

public class BathingSitesView extends TableLayout {

    private final String TEXT = getResources().getString(R.string.bathing_site_text);
    private TableRow row2;

    public BathingSitesView(final Context context) {
        super(context);
        init(context, null);
    }

    /**
     * function to initialize and set the layout
     * @param context the application context
     * @param attributeSet any attribute sets
     */
    private void init(Context context, @Nullable AttributeSet attributeSet) {
        final TableRow row1 = new TableRow(context);
        row1.addView(createImageView());
        row1.setGravity(Gravity.CENTER_HORIZONTAL);
        row2 = new TableRow(context);
        row2.addView(createTextView(), 0);
        row2.setGravity(Gravity.CENTER_HORIZONTAL);
        addView(row1);
        addView(row2);
    }

    /**
     * Create an image View
     * @return ImageView
     */
    private ImageView createImageView() {
        ImageView img = new ImageView(getContext());
        img.setImageResource(R.drawable.swimming_pool);
        return img;
    }

    /**
     * Create a textView
     * @return a TextView object
     */
    private TextView createTextView() {
        TextView textView = new TextView(getContext());
        textView.setTextSize(20f);
        textView.setTextColor(Color.BLACK);
        String text = "0 " + TEXT;
        textView.setText(text);
        return textView;
    }

    public BathingSitesView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Set the number of bathing sites and update the view
     * @param sites the number of bathing sites
     */
    public void setNumberOfBathingSites(final int sites) {
        String s = sites + " " + TEXT;
        TextView view = getSitesView();
        if (view != null) {
            view.setText(s);
            postInvalidate();
        }
    }

    /**
     * Get the field where the number of bathing sites is displayed
     * @return a TextView object
     */
    private TextView getSitesView() {
        return (TextView) row2.getChildAt(0);
    }

    /**
     * Get the number of bathing sites present
     * @return number of bathing sites
     */
    public int getNumberOfBathingSites() {
        String text[] = getSitesView().getText().toString().split(" ");
        return text.length > 0 ? Integer.parseInt(text[0]) : 0;
    }
}
