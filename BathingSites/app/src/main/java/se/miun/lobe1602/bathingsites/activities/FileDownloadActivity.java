package se.miun.lobe1602.bathingsites.activities;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.DownloadListener;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import se.miun.lobe1602.bathingsites.R;
import se.miun.lobe1602.bathingsites.database.BathingSiteRepository;
import se.miun.lobe1602.bathingsites.helpers.BathingSite;
import se.miun.lobe1602.bathingsites.helpers.Globals;
import se.miun.lobe1602.bathingsites.helpers.GoogleMapData;
import se.miun.lobe1602.bathingsites.helpers.WebViewHelper;

/**
 *
 */
public class FileDownloadActivity extends AppCompatActivity {

    private WebView webView;
    private String downloadLink;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.settings_preferences, false);
        setContentView(R.layout.activity_file_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        webView = (WebView) findViewById(R.id.download_file_view);
        downloadLink = Globals.getPreference(Globals.BATHING_SITES_URL, this);
        loadDownloadSite();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(FileDownloadActivity.this, MainSettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * To load the download site, the following conditions must be satisfied
     * <p>
     * 1. There is internet
     * <p>
     * 2. There is permission to write to external storage
     */
    private void loadDownloadSite() {
        webView.setWebViewClient(new WebViewHelper());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(downloadLink);
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(final String url, final String userAgent,
                                        final String contentDisposition, final String mimetype,
                                        final long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setMimeType("text/comma-separated-values");
                request.allowScanningByMediaScanner();
                // notify the user when the download is completed
                request.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                if (isExternalStorageWritable()) {
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                        Globals.BATHING_SITES_FILE);
                    DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

                    dm.enqueue(request);


                    Globals.toast(getApplicationContext(), "Download and reading of file");

                    receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();
                            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                                Globals.toast(getApplicationContext(), "File reading completed");
                                processFile();
                            }
                        }

                    };
                    registerReceiver(receiver,
                        new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                } else {
                    Globals.toast(getApplicationContext(), "No space to save file");
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanup();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cleanup();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        cleanup();
    }

    private void cleanup() {
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void processFile() {
        ReadFileAsyncTask readFile = new ReadFileAsyncTask();
        Globals.toastLong(this, "Reading file contents...");
        readFile.execute();

        try {
            ArrayList<BathingSite> bathingSites = readFile.get();
            if (!bathingSites.isEmpty()) {

                BathingSiteRepository repository =
                    new BathingSiteRepository(getApplicationContext());

                for (BathingSite bsite : bathingSites) {
                    Globals.toast(getApplicationContext(),
                        "Saving bathing sites to database. Please wait" + "...");
                    repository.insert(bsite);
                }
                Globals.addToPreference(Globals.BATHING_SITES,
                    Integer.toString(repository.numberOfBathingSites()), getApplicationContext());

                Globals.toastLong(getApplicationContext(), "Saving of bathing sites completed");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ReadFileAsyncTask extends AsyncTask<Void, Void, ArrayList<BathingSite>> {
        private ArrayList<BathingSite> bathingSites;
        ProgressDialog pd;
        AlertDialog.Builder progress;

        ReadFileAsyncTask() {
            super();
            Globals.toastLong(FileDownloadActivity.this, "Reading file contents...");
            bathingSites = new ArrayList<>();
        }

        @Override
        protected ArrayList<BathingSite> doInBackground(final Void... voids) {
            BufferedReader buffer;
            try {
                String line = "";
                File file = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    Globals.BATHING_SITES_FILE);
                if (file.exists()) {
                    buffer = new BufferedReader(
                        new InputStreamReader(new FileInputStream(file.getAbsoluteFile()),
                            "ISO-8859-1"));

                    while ((line = buffer.readLine()) != null) {
                        String splits[] = line.split(",");

                        double longitude =
                            Double.parseDouble(splits[0].replaceAll("\"", "").trim());
                        double latitude = Double.parseDouble(splits[1].replaceAll("\"", "").trim());
                        String name = splits[2].replaceAll("\"", "").trim();
                        String address = null;
                        if (splits.length == 4) {
                            address = splits[3].replaceAll("\"", "").trim();
                        }
                        BathingSite site =
                            new BathingSite(new GoogleMapData(name, address, longitude, latitude));
                        bathingSites.add(site);

                        if (isCancelled()) {
                            Globals.deleteFile();
                            break;
                        }
                    }
                    if (Globals.deleteFile()) {
                        Globals.toast(FileDownloadActivity.this, file.getName() + " Deleted");
                    }
                    buffer.close();
                } else {
                    Log.i("Here ==>>>>", "No file");
                }

            } catch (Exception e) {
                Log.d(getClass().getSimpleName().toUpperCase(), e.getMessage());
            }
            return bathingSites;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(FileDownloadActivity.this);
            pd.setTitle("Downloading");
            pd.setMessage("Please wait...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
        }

        @Override
        protected void onPostExecute(final ArrayList<BathingSite> bathingSites) {
            super.onPostExecute(bathingSites);
            String msg =
                "File reading completed with [" + bathingSites.size() + "] bathing " + "sites";

            AlertDialog.Builder update = new AlertDialog.Builder(FileDownloadActivity.this);
            update.setTitle("End of file reading");
            update.setMessage(msg);
            update.setPositiveButton("Close", new OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    if (bathingSites.size() > 0) {
                        Globals.toastLong(getApplicationContext(), "Saving files to database...");
                    }
                    dialog.dismiss();
                }
            });
            if (pd.isShowing()) {
                pd.dismiss();
            }
            update.create().show();
        }
    }
}
