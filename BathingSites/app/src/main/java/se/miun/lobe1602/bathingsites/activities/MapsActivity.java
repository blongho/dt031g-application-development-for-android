package se.miun.lobe1602.bathingsites.activities;

import android.Manifest;
import android.Manifest.permission;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import se.miun.lobe1602.bathingsites.R;
import se.miun.lobe1602.bathingsites.database.BathingSiteRepository;
import se.miun.lobe1602.bathingsites.helpers.Globals;
import se.miun.lobe1602.bathingsites.helpers.GoogleMapData;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback, OnInfoWindowClickListener {

    private final String TAG = getClass().getSimpleName().toUpperCase();
    private GoogleMap mMap;
    private List<GoogleMapData> mapData;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int LOCATION_ACCESS_CODE = 999;
    private int bathingSiteRadius;
    TextView radiusText;
    private FusedLocationProviderClient fusedLocationClient;
    private final float DEFAULT_ZOOM = 15f;
    private Location currentLocation;
    List<GoogleMapData> sitesWithinRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.settings_preferences, false);
        setContentView(R.layout.activity_maps_page);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment =
            (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        BathingSiteRepository repository = new BathingSiteRepository(this);

        mapData = repository.getGoogleMapData();

        radiusText = (TextView) findViewById(R.id.radius);
        radiusText.setText(radiusText(getBathingSiteRadius() + ""));

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        changeLocationRadiusListener();

    }

    /**
     * Get the device location
     */
    public void getDeviceLocation() {
        if (ActivityCompat.checkSelfPermission(this, FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            setCurrentLocation(location);
                            moveCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                                DEFAULT_ZOOM);
                            mMap.addMarker(new MarkerOptions().position(
                                new LatLng(location.getLatitude(), location.getLongitude())).title(
                                "Your current location: [" + location.getLatitude() + ", " +
                                    location.getLongitude() + "]"));
                            getLocationWithinRadius();

                        } else {
                            new AlertDialog.Builder(MapsActivity.this)
                                .setTitle("GPS signal " + "turned off")
                                .setMessage("Please turn on your device location")
                                .setPositiveButton("Ok", new OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface dialog,
                                                        final int which) {
                                        startActivity(new Intent(
                                            android.provider.Settings
                                                .ACTION_LOCATION_SOURCE_SETTINGS));
                                    }
                                }).create().show();
                        }
                    }
                });

        } else {
            requestLocationPermission();
        }
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(final Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " +
            latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    /**
     * Listens to and changes the radius decided by the user using the slidebar
     */
    private void changeLocationRadiusListener() {
        final SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(final SeekBar seekBar, final int progress,
                                          final boolean fromUser) {
                setBathingSiteRadius(progress);
                radiusText.setText(radiusText(bathingSiteRadius + ""));
                setBathingSiteRadius(progress);

            }

            @Override
            public void onStartTrackingTouch(final SeekBar seekBar) {
                // nothing to implement here
            }

            @Override
            public void onStopTrackingTouch(final SeekBar seekBar) {
                int progress = seekBar.getProgress();
                setBathingSiteRadius(progress);
                Globals.addToPreference(Globals.SITE_RADIUS, String.valueOf(progress),
                    MapsActivity.this);
                radiusText.setText(radiusText(bathingSiteRadius + ""));
                setBathingSiteRadius(progress);
            }
        });
    }

    /**
     * Manipulates the map once available. This callback is triggered when the map is ready to be
     * used. This is where we can add markers or lines, add listeners or move the camera. In this
     * case, we just add a marker near Sydney, Australia. If Google Play services is not installed
     * on the device, the user will be prompted to install it inside the SupportMapFragment. This
     * method will only be triggered once the user has installed Google Play services and returned
     * to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getDeviceLocation();

        mMap.setOnInfoWindowClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);

        if (mapData != null) {
            for (GoogleMapData data : mapData) {
                LatLng site = new LatLng(data.getLatitude(), data.getLongitude());
                String text = data.toString();
                mMap.addMarker(new MarkerOptions().position(site).title(text));
            }
        } else {
            mMap.addMarker(
                new MarkerOptions().position(new LatLng(0, 0)).title("No bathing " + "site"));
        }

        createLocationRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBathingSiteRadius();
        radiusText.setText(radiusText(getBathingSiteRadius() + ""));

    }

    @Override
    protected void onPause() {
        super.onPause();
        Globals.addToPreference(Globals.SITE_RADIUS, getBathingSiteRadius() + "", MapsActivity
            .this);

    }

    /**
     * Request permission data if permission is not set
     */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this, FINE_LOCATION)) {
            new AlertDialog.Builder(this).setTitle(R.string.permission_title)
                .setMessage(R.string.location_rationale)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        sendLocationRequest();
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                    Globals.toast(getApplicationContext(),
                        getResources().getString(R.string.permission_denied));

                }
            }).create().show();
        } else {
            sendLocationRequest();
        }
    }

    private String radiusText(String value) {
        return "Current radius: " + value + " km";
    }

    /**
     * Send request to get location permission
     */
    private void sendLocationRequest() {
        ActivityCompat
            .requestPermissions(MapsActivity.this, new String[]{permission.ACCESS_FINE_LOCATION},
                LOCATION_ACCESS_CODE);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        if (requestCode == LOCATION_ACCESS_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getDeviceLocation();
            } else {
                Globals.toastLong(MapsActivity.this,
                    getResources().getString(R.string.permission_denied));
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public int getBathingSiteRadius() {
        String radius =
            Globals.getPreference(Globals.SITE_RADIUS, MapsActivity.this).split(" " + "")[0];
        return Integer.valueOf(radius);
    }

    public void setBathingSiteRadius(final int bathingSiteRadius) {

        this.bathingSiteRadius = bathingSiteRadius;
    }

    private void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        marker.showInfoWindow();
    }


    private double siteDistance(double lat_a, double long_a, double lat_b, double long_b) {
        Location selected_location = new Location("locationA");
        selected_location.setLatitude(lat_a);
        selected_location.setLongitude(long_a);
        Location near_locations = new Location("locationB");
        near_locations.setLatitude(lat_b);
        near_locations.setLongitude(long_b);
        return selected_location.distanceTo(near_locations);
    }

    private List<GoogleMapData> getLocationWithinRadius() {
        int radius = getBathingSiteRadius();
        sitesWithinRadius = new ArrayList<>();
        double lat_a = getCurrentLocation().getLatitude();
        double long_a = getCurrentLocation().getLongitude();
        if (mapData != null) {
            for (GoogleMapData data : mapData) {
                if (siteDistance(lat_a, long_a, data.getLatitude(), data.getLongitude()) <=
                    (float) radius) {
                    sitesWithinRadius.add(data);
                }
            }
            return sitesWithinRadius;
        }
        return null;
    }

}
