/**
 * BathingSite.java
 * @author Bernard Che Longho(lobe1602) A helper class to create a bathing class instance
 * @since 2018-05-23
 */
package se.miun.lobe1602.bathingsites.helpers;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.Nullable;

import io.reactivex.annotations.NonNull;

@Entity(tableName = Globals.DATABASE_TABLE)
public class BathingSite {
    @PrimaryKey(autoGenerate = true)
    public long id;
    /** Name of the the site */
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    /** Description of the site e.g does it have wifi? etc */
    @Nullable
    @ColumnInfo(name = "description")
    private String description;
    /** The physical address of the bathing site */
    @ColumnInfo(name = "address")
    private String address;
    /** The date in which the site was added by the user */
    @Nullable
    @ColumnInfo(name = "date")
    private String date;
    /** the latitude and longitude of the physical address */
    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    /** the temperature of the location/bathing site */
    @ColumnInfo(name = "temperature")
    private double temperature;
    /** User rating in a scale of 1-5 */
    @ColumnInfo(name = "rating")
    private double rating;

    @Ignore
    private GoogleMapData mapData;

    /**
     * Empty constructor
     */
    @Ignore
    public BathingSite() { mapData = new GoogleMapData();}

    /**
     * @param name the name of the bathing place
     * @param address he address of the bathing place
     * @param description description of the bathing place
     * @param longitude the longitude
     * @param latitude the latitude
     * @param temperature the temperature of the water
     * @param rating the rating by the user
     * @param date the date that temperature was measured
     */

    public BathingSite(String name, String address, String description, double longitude,
                       double latitude, double rating, double temperature, String date) {
        mapData = new GoogleMapData(name, address, longitude, latitude);
        setName(name);
        setDescription(description);
        setAddress(address);
        setRating(rating);
        setDate(date);
        setTemperature(temperature);
        setLongitude(longitude);
        setLatitude(latitude);
    }

    @Ignore
    public BathingSite(GoogleMapData mapData, String description, double rating, double temperature,
                       String date) {
        setMapData(mapData);
        setLatitude(mapData.getLatitude());
        setLongitude(mapData.getLongitude());
        setName(mapData.getName());
        setAddress(mapData.getAddress());
        setDescription(description);
        setRating(rating);
        setTemperature(temperature);
        setDate(date);
    }

    /**
     * Constructor reading from file
     **/
    @Ignore
    public BathingSite(GoogleMapData mapData) {
        setLongitude(mapData.getLongitude());
        setLatitude(mapData.getLatitude());
        setName(mapData.getName());
        setAddress(mapData.getAddress());
    }

    /** Set the name of the bathing site */
    public void setName(final String name) {
        this.name = name;

    }

    /** Set the description of the bathing site */
    public void setDescription(final String description) {
        this.description = description;
    }

    /** Get the address (name of town) of the bathing site */
    public void setAddress(final String address) {
        this.address = address;
    }

    /** Set the rating of the bathing site */
    public void setRating(final double rating) {
        if (rating >= 1 && rating <= 5) {
            this.rating = rating;
        }
    }

    /** Let the user set the date that the bathing site was entered */
    public void setDate(final String date) {
        this.date = date;
    }

    /** Set the temperature of the bathing site */
    public void setTemperature(final double temperature) {
        this.temperature = temperature;
    }

    /** Get the name of the bathing site */
    public String getName() {
        return name;
    }

    /** Get the description of the bathing site */
    public String getDescription() {
        return description;
    }

    /** Get the address (name of town) of the bathing site */
    public String getAddress() {
        return address;
    }

    /** Get the rating(review points 1- 5) of the bathing site */
    public double getRating() {
        return rating;
    }

    /** Get the temperature of the bathing site */
    public double getTemperature() {
        return temperature;
    }

    /** Get the date the bathing site was entered into the system by the uer */
    public String getDate() {
        return date;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    /**
     * Make a string representation of the Bathing site object
     * @return Info about a bathing site object
     */
    @Override
    public String toString() {
        String string = (name != null ? "\nName: " + name : "");
        string += "\n";
        string += (address != null ? "Address: " + address : "");
        string += "\n";
        string += (description != null ? "Description : " + description : "");
        string += "\n";
        string +=
            "Longitude: " + longitude + "\nLatitude: " + latitude + "\nWater temp.: " + temperature;
        string += (date != null ? "\nDate measured: " + date : "");
        string += "\n";
        return string;
    }

    public GoogleMapData getMapData() {
        return mapData;
    }

    public void setMapData(final GoogleMapData mapData) {
        this.mapData = mapData;
    }
}
