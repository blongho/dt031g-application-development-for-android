package se.miun.lobe1602.bathingsites.database;

/**
 * This class handles every connection to the database tables <br>
 * <p>
 * Since this project requires only the number of bathing sites present, i will limit this class to
 * just the basics.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import se.miun.lobe1602.bathingsites.helpers.BathingSite;
import se.miun.lobe1602.bathingsites.helpers.GoogleMapData;


public class BathingSiteRepository {

    private BathingSiteDao bDao;
    Context context;

    public BathingSiteRepository(Context context) {
        this.context = context;
        BathingSiteDb db = BathingSiteDb.getDatabase(context);
        bDao = db.bathingSiteDao();
    }

    public List<BathingSite> getBathingSites() {
        return bDao.getAllSites();
    }

    public BathingSite getBathingSite(String name) {
        return bDao.getSiteByName(name);
    }

    public int deleteBathingSite(BathingSite site) {
        return bDao.deleteSites(site);
    }

    public List<GoogleMapData> getGoogleMapData() {return bDao.getGoogleMapData();}

    public int numberOfBathingSites() {return getBathingSites().size();}

    public int insert(BathingSite bathingSite) {

        if (!siteAlreadyExits(bathingSite)) {
            try {
                InsertBathingSiteAsyncTask insert = new InsertBathingSiteAsyncTask(bDao);
                insert.execute(bathingSite);
                return bDao.getAllSites().size();
            } catch (Exception e) {
                Log.d(getClass().getSimpleName().toUpperCase(), e.getMessage());
            }
        }
        return 0;
    }

    /**
     * Do a check that the site exists or note before
     * @param site the new site to be added to the database
     * @return true if site exists
     */
    public boolean siteAlreadyExits(BathingSite site) {
        final List<BathingSite> bathingSites = getBathingSites();
        for (BathingSite bathingSite : bathingSites) {
            if (isSameBathingSite(site, bathingSite)) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        BathingSiteDb.destroyInstance();
    }

    private static class InsertBathingSiteAsyncTask extends AsyncTask<BathingSite, String, Long> {

        private BathingSiteDao asyncBathingSite;


        InsertBathingSiteAsyncTask(final BathingSiteDao bDao) {
            asyncBathingSite = bDao;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(final BathingSite... bathingSites) {
            return asyncBathingSite.insert(bathingSites[0]);
        }
    }

    private boolean isSameBathingSite(BathingSite a, BathingSite b) {
        return (a.getLatitude() == b.getLatitude()) && (a.getLongitude() == b.getLongitude());
    }
}
