package se.miun.lobe1602.bathingsites.activities;
/**
 * @file MainActivity.ja The entry point of the application. Here the number of bathing sites in
 *     the system are shown to the user and the user has the opportunity to add new bathing sites
 *     and to make some settings in the settings menu.
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-05-22
 */

import android.Manifest.permission;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import se.miun.lobe1602.bathingsites.R;
import se.miun.lobe1602.bathingsites.helpers.Globals;
import se.miun.lobe1602.bathingsites.helpers.GoogleMapData;
import se.miun.lobe1602.bathingsites.views.BathingSitesView;

public class MainActivity extends AppCompatActivity {

    private BathingSitesView bathingSitesView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.settings_preferences, false);
        bathingSitesView = (BathingSitesView) findViewById(R.id.bathing_site_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(), NewBathingSiteActivity.class));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Globals.addToPreference(Globals.BATHING_SITES,
            Integer.toString(bathingSitesView.getNumberOfBathingSites()), this);
    }

    // get preferences here
    @Override
    protected void onResume() {
        super.onResume();
        String sites = Globals.getPreference(Globals.BATHING_SITES, this);
        if (!sites.isEmpty()) {
            int bathingSites = Integer.parseInt(sites);
            bathingSitesView.setNumberOfBathingSites(bathingSites);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, MainSettingsActivity.class));
                return true;
            case R.id.downloadSiteButton:
                doDownload();
                return true;
            case R.id.mapButton:
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * To load the download site, the following conditions must be satisfied
     * <p>
     * 1. There is internet
     * <p>
     * 2. There is permission to write to external storage
     **/
    private void doDownload() {
        if (Globals.hasNetworkConnection(getApplication())) {
            // Check for permission when there is internet
            if (VERSION.SDK_INT >= 23) {
                if (ContextCompat
                    .checkSelfPermission(MainActivity.this, permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED) {

                    goToDownloadActivity();
                } else {
                    requestStoragePermission();
                }
            } else {
                // Permission has already been granted
                goToDownloadActivity();
            }
        } else { // no internet
            Globals.toast(this,
                "Oops, you need access to the internet to download " + "bathing sites");
        }

    }

    /**
     * Redirect the user to the FileDownload acivity
     */
    private void goToDownloadActivity() {
        Intent downloadIntent = new Intent(MainActivity.this, FileDownloadActivity.class);
        startActivity(downloadIntent);
    }


    /**
     * Request permission for writing to file
     */
    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
            permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this).setTitle(R.string.permission_title)
                .setMessage(R.string.storage_rationale)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{permission.WRITE_EXTERNAL_STORAGE},
                            Globals.FILE_WRITE_PERMISSION_CODE);
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                    Globals.toast(getApplicationContext(),
                        getResources().getString(R.string.permission_denied));
                }
            }).create().show();

        } else {
            ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{permission.WRITE_EXTERNAL_STORAGE},
                Globals.FILE_WRITE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        if (requestCode == Globals.FILE_WRITE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                goToDownloadActivity();
            } else {
                Globals.toast(MainActivity.this, "Permission denied");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}


