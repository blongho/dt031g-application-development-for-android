package se.miun.lobe1602.bathingsites.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import se.miun.lobe1602.bathingsites.helpers.BathingSite;
import se.miun.lobe1602.bathingsites.helpers.Globals;

@Database(entities = {BathingSite.class}, version = Globals.DATABASE_VERSION)
public abstract class BathingSiteDb extends RoomDatabase {

    public abstract BathingSiteDao bathingSiteDao();

    private static BathingSiteDb INSTANCE;

    static BathingSiteDb getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (BathingSiteDb.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                        Room.databaseBuilder(context.getApplicationContext(), BathingSiteDb.class,
                            Globals.DATABASE_NAME).allowMainThreadQueries()
                            .fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Free the resource
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }
}