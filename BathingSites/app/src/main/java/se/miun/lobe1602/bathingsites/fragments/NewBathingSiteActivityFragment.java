package se.miun.lobe1602.bathingsites.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import se.miun.lobe1602.bathingsites.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewBathingSiteActivityFragment extends Fragment {

    public NewBathingSiteActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_bathing_site, container, false);
    }
}
