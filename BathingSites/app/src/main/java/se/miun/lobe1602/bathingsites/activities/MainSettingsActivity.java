package se.miun.lobe1602.bathingsites.activities;
/**
 * @file MainSettingsActivity.java <br>
 *     <p>
 *     The settings for the app are made using this activity
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-05-26
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import se.miun.lobe1602.bathingsites.preference.SettingsFragment;

public class MainSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().addToBackStack(null)
            .replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
